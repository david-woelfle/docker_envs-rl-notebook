#!/bin/bash

# This is done by the TF image too, it should be something with autocomplete
source /etc/bash.bashrc

echo ""
echo "Starting up RL Notebook container."
echo -e "\e[31mContrary to the message by Tensorflow above, this container should always be run as root. Jupyter is executed with user_id matching the owner of the notebooks directory.\e[0m"
echo ""

# Start up a VNC server that provides a virtual display. This allows the user
# to see the output of the environments render functions.
VNCPASS=$(printf "%06d" $((1 + RANDOM * 27)))
echo "Starting VNC with PIN: $VNCPASS"
# Use -SecurityTypes None to deactivate password checking
expect -c "spawn vnc4server -geometry 640x480; expect Password; send $VNCPASS\r; expect Verify; send $VNCPASS\r; expect eof"  2&> /dev/null
export DISPLAY="$(ls /root/.vnc/*:1.pid | cut -d / -f 4 | cut -d . -f 1)"
echo "Started VNC Server for DISPLAY: $DISPLAY"

# This is the place where the user should place their work.
# Create the folder if it hasn't been mounted as volume.
mkdir -p /tf/notebooks

# Get the user that owns the notebooks directory. From now on we execute all 
# the remaining tools as that user to prevent permission issues while accessing
# the notebooks directory  
NB_USER="$(ls -ld /tf | cut -d " " -f 3)"

# Check if NB_USER exists, if not NB_USER is a user id. Create new account so
# we can start processes as this user.
if id -u $NB_USER > /dev/null
# if [ $? -ne 0 ]
then
    useradd -d /tf/notebooks -u $NB_USER nbuser
    NB_USER=nbuser
fi

# Also expose the display env var to the user running the notebook
runuser -l $NBUSER -c "export DISPLAY=$DISPLAY"

# Start up Tensorboard.
runuser -l $NBUSER -c "tensorboard --bind_all --logdir /tf/notebooks/tb_logs 2&>1 /dev/null" &  
echo "Started Tensorboard on port 6006"
echo ""

# Start the notebook. Also expose the display env var to the user running the notebook.
# The later is required for the rendering process.
echo "DISPLAY=$DISPLAY jupyter notebook --notebook-dir=/tf --ip 0.0.0.0 --no-browser --allow-root"
runuser -l $NBUSER -c "DISPLAY=$DISPLAY jupyter notebook --notebook-dir=/tf --ip 0.0.0.0 --no-browser --allow-root"
