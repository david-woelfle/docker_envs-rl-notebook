These are the building instructions for a docker image I use for developing reinforcement learning stuff.

# Run the image locally 

For CPU:

```
docker build --tag=rl-notebook:cpu-py3 -f Dockerfile.cpu_py3 .
docker run --rm -it  -p 8888:8888 -p 5901:5901 -p 6006:6006 -v $(realpath ~/notebooks):/tf/notebooks rl-notebook:cpu-py3
```
for GPU:
```
docker build --tag=rl-notebook:gpu-py3 -f Dockerfile.gpu_py3 .
docker run --rm -it  -p 8888:8888 -p 5901:5901 -p 6006:6006 -v $(realpath ~/notebooks):/tf/notebooks rl-notebook:gpu-py3
```
Jupyter Token and VNC Pin can be found in the container logs.

# File structure in container

Jupyter will expect notebooks in `/tf/notebooks`.  
Tensorboard will look for data in `/notebooks/tb_logs`.

